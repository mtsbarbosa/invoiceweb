DROP TABLE IF EXISTS invoice_item;
DROP TABLE IF EXISTS item;
DROP TABLE IF EXISTS invoice;
DROP TABLE IF EXISTS customer;

CREATE TABLE customer
(
    id   BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(150) NOT NULL
);

INSERT INTO customer (name)
VALUES ('Paul Fischer'),
       ('John McDonald'),
       ('Demetrius Augustus');

CREATE TABLE invoice
(
    ref         VARCHAR(10) PRIMARY KEY,
    date        DATE         NOT NULL,
    amount      DECIMAL      NOT NULL,
    currency    VARCHAR(3) NOT NULL,
    customer_id BIGINT       NOT NULL,
    foreign key(customer_id) references customer(id)
);
INSERT INTO invoice (ref, date, amount, currency, customer_id)
    VALUES ('D345556560', '2020-01-24', 350.43, 'EUR', (SELECT id FROM customer WHERE name = 'Paul Fischer'));
INSERT INTO invoice (ref, date, amount, currency, customer_id)
    VALUES ('D345556564', '2020-01-24', 2456.09, 'USS', (SELECT id FROM customer WHERE name = 'John McDonald'));
INSERT INTO invoice (ref, date, amount, currency, customer_id)
    VALUES ('D845556567', '2020-02-05', 12, 'EUR', (SELECT id FROM customer WHERE name = 'Paul Fischer'));

CREATE TABLE item
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(150) NOT NULL UNIQUE
);

INSERT INTO item (name)
    VALUES  ('P-S.RD-Y');
INSERT INTO item (name)
    VALUES  ('P-S.PS-Y');
INSERT INTO item (name)
    VALUES  ('TCA-NS');
INSERT INTO item (name)
    VALUES  ('P-S.PCODEAIICEBERG-Y');
INSERT INTO item (name)
    VALUES  ('C-S.RRR-Y');

CREATE TABLE invoice_item
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY,
    invoice_ref VARCHAR(10) NOT NULL,
    item_id     BIGINT NOT NULL,
    quantity    INT NOT NULL DEFAULT (1),
    foreign key(invoice_ref) references invoice(ref),
    foreign key(item_id) references item(id)
);

INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D345556560',(SELECT id FROM item WHERE name = 'TCA-NS'), 1);
INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D345556560',(SELECT id FROM item WHERE name = 'P-S.PS-Y'), 6);
INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D345556564',(SELECT id FROM item WHERE name = 'P-S.RD-Y'), 1);
INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D845556567',(SELECT id FROM item WHERE name = 'TCA-NS'), 10);
INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D845556567',(SELECT id FROM item WHERE name = 'P-S.PCODEAIICEBERG-Y'), 5);
INSERT INTO invoice_item (invoice_ref, item_id, quantity)
VALUES
    ('D845556567',(SELECT id FROM item WHERE name = 'C-S.RRR-Y'), 1);