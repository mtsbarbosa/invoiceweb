package net.mtsbarbosa.invoiceweb.lang;

public class DuplicatedException extends Exception{
    public DuplicatedException(String message) {
        super(message);
    }
}
