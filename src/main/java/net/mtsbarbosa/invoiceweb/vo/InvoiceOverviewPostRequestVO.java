package net.mtsbarbosa.invoiceweb.vo;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceOverviewPostRequestVO extends InvoiceOverviewItemVO{
    public InvoiceOverviewPostRequestVO(String customerName, String date, String ref, BigDecimal amount, String itemName, int quantity, String currency) {
        super(customerName, date, ref, amount, itemName, quantity, currency);
    }
}
