package net.mtsbarbosa.invoiceweb.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BaseListResponseVO<T extends Object> extends BaseResponseVO{

    private List<T> body = new ArrayList<>();

    public List<T> getBody() {
        return body;
    }

    public void setBody(List<T> body) {
        this.body = body;
    }
}
