package net.mtsbarbosa.invoiceweb.vo;

import java.util.HashSet;
import java.util.Set;

public class BaseResponseVO {
    protected Set<String> messages = new HashSet<>();

    public Set<String> getMessages() {
        return messages;
    }

    public void setMessages(Set<String> messages) {
        this.messages = messages;
    }
}
