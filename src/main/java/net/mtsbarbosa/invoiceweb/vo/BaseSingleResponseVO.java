package net.mtsbarbosa.invoiceweb.vo;

public class BaseSingleResponseVO<T extends Object> extends BaseResponseVO{

    private T body;

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
