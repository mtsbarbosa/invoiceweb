package net.mtsbarbosa.invoiceweb.vo;

import net.mtsbarbosa.invoiceweb.util.StringUtil;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InvoiceOverviewItemVO {

    @NotNull
    @Length(min = 3, max = 150)
    private String customerName;

    private String transactionType = StringUtil.capitalizeFirst(TransactionTypeEnum.INVOICE.name());

    @NotNull
    @Pattern(regexp = "^([0-2][0-9]|(3)[0-1]).(((0)[0-9])|((1)[0-2])).\\d{4}$")
    private String date;

    @NotNull
    @Length(min = 10, max = 10)
    private String ref;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("9999999999.99")
    private BigDecimal amount;

    @NotNull
    @Length(min = 3, max = 150)
    private String itemName;

    @NotNull
    @Min(1)
    private int quantity = 1;

    @NotNull
    @Length(min = 3, max = 150)
    private String currency;

    public InvoiceOverviewItemVO(){}

    public InvoiceOverviewItemVO(String customerName, Date date, String ref, BigDecimal amount, String itemName, int quantity, String currency) {
        this();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        this.customerName = customerName;
        this.date = dateFormat.format(date);
        this.ref = ref;
        this.amount = amount;
        this.itemName = itemName;
        this.quantity = quantity;
        this.currency = currency;
    }

    public InvoiceOverviewItemVO(String customerName, String date, String ref, BigDecimal amount, String itemName, int quantity, String currency) {
        this();
        this.customerName = customerName;
        this.date = date;
        this.ref = ref;
        this.amount = amount;
        this.itemName = itemName;
        this.quantity = quantity;
        this.currency = currency;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
