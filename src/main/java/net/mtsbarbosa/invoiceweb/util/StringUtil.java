package net.mtsbarbosa.invoiceweb.util;

public class StringUtil {
    public static String capitalizeFirst(String text){
        if(text != null && !text.isEmpty()){
            return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
        }
        return "";
    }
}
