package net.mtsbarbosa.invoiceweb.service;

import net.mtsbarbosa.invoiceweb.lang.DuplicatedException;
import net.mtsbarbosa.invoiceweb.model.*;
import net.mtsbarbosa.invoiceweb.repository.CustomerRepository;
import net.mtsbarbosa.invoiceweb.repository.InvoiceItemRepository;
import net.mtsbarbosa.invoiceweb.repository.InvoiceRepository;
import net.mtsbarbosa.invoiceweb.repository.ItemRepository;
import net.mtsbarbosa.invoiceweb.vo.InvoiceOverviewItemVO;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private InvoiceItemRepository invoiceItemRepository;

    @Transactional(readOnly = true)
    public List<InvoiceOverviewItemVO> getOverviewInvoices() {
        Iterable<Invoice> invoices = invoiceRepository.findAll();
        List<InvoiceOverviewItemVO> overviewItems = new ArrayList<>();
        invoices.forEach(invoice -> {
            invoice.getInvoiceItems()
                    .stream()
                    .forEach(invoiceItem -> {
                        overviewItems.add(
                            new InvoiceOverviewItemVO(
                                    invoice.getCustomer().getName(),
                                    invoice.getDate(),
                                    invoice.getRef(),
                                    invoice.getAmount(),
                                    invoiceItem.getItem().getName(),
                                    invoiceItem.getQuantity(),
                                    invoice.getCurrency().getDisplayName()
                            ));
            });
        });

        return overviewItems;
    }

    @Transactional(rollbackFor = Exception.class)
    public InvoiceOverviewItemVO insertOverviewInvoice(InvoiceOverviewItemVO invoiceOverviewItemVO) throws Exception {
        MutableBoolean duplicatedOverview = new MutableBoolean(false);
        Optional<Item> item = processItemFromInvoiceOverview(invoiceOverviewItemVO, duplicatedOverview);
        Optional<Customer> customer = processCustomerFromInvoiceOverview(invoiceOverviewItemVO);
        Optional<Invoice> invoice = processInvoiceFromInvoiceOverview(invoiceOverviewItemVO, customer, duplicatedOverview);
        if(duplicatedOverview.isTrue()){
            throw new DuplicatedException("Invoice Overview is duplicated");
        }
        processInvoiceItemFromInvoiceOverview(invoice.get(), item.get());
        return invoiceOverviewItemVO;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected void processInvoiceItemFromInvoiceOverview(Invoice invoice, Item item) {
        Optional<InvoiceItem> invoiceItem = invoiceItemRepository.findByInvoiceAndItem(invoice, item);
        if(!invoiceItem.isPresent()){
            InvoiceItem invoiceItemModel = new InvoiceItem();
            invoiceItemModel.setInvoice(invoice);
            invoiceItemModel.setItem(item);
            invoiceItemRepository.save(invoiceItemModel);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected Optional<Invoice> processInvoiceFromInvoiceOverview(InvoiceOverviewItemVO invoiceOverviewItemVO, Optional<Customer> customer, MutableBoolean duplicatedOverview) throws ParseException {
        Optional<Invoice> invoice = invoiceRepository.findById(invoiceOverviewItemVO.getRef());
        if(!invoice.isPresent()){
            Invoice invoiceModel = new Invoice();
            invoiceModel.setRef(invoiceOverviewItemVO.getRef());
            invoiceModel.setDate(new SimpleDateFormat("dd.MM.yyyy").parse(invoiceOverviewItemVO.getDate()));
            invoiceModel.setAmount(invoiceOverviewItemVO.getAmount());
            invoiceModel.setCurrency(
                    Arrays.asList(CurrencyEnum.values())
                            .stream()
                            .filter(currencyEnum -> currencyEnum.getDisplayName().equalsIgnoreCase(invoiceOverviewItemVO.getCurrency()))
                            .findFirst()
                            .get());
            invoiceModel.setCustomer(customer.get());
            invoice = Optional.of(invoiceRepository.save(invoiceModel));
            duplicatedOverview.setFalse();
        }
        return invoice;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected Optional<Customer> processCustomerFromInvoiceOverview(InvoiceOverviewItemVO invoiceOverviewItemVO) {
        Optional<Customer> customer = customerRepository.findFirstByName(invoiceOverviewItemVO.getCustomerName());
        if(!customer.isPresent()){
            Customer customerModel = new Customer();
            customerModel.setName(invoiceOverviewItemVO.getCustomerName());
            customer = Optional.of(customerRepository.save(customerModel));
        }
        return customer;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected Optional<Item> processItemFromInvoiceOverview(InvoiceOverviewItemVO invoiceOverviewItemVO, MutableBoolean duplicatedOverview) {
        Optional<Item> item = itemRepository.findFirstByName(invoiceOverviewItemVO.getItemName());
        if(!item.isPresent()){
            Item itemModel = new Item();
            itemModel.setName(invoiceOverviewItemVO.getItemName());
            item = Optional.of(itemRepository.save(itemModel));
        }else{
            //may be duplicated depending on processInvoiceFromInvoiceOverview
            duplicatedOverview.setTrue();
        }
        return item;
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteCascadeInvoice(Invoice invoice){
        invoice = invoiceRepository.findById(invoice.getRef()).get();
        invoiceItemRepository.deleteAll(invoice.getInvoiceItems());
        invoiceRepository.delete(invoice);
    }
}
