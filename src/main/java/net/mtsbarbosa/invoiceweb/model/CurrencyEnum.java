package net.mtsbarbosa.invoiceweb.model;

public enum CurrencyEnum {
    USS("United States Dollar"),
    EUR("Euro");

    private String displayName;

    CurrencyEnum(String displayName){
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
