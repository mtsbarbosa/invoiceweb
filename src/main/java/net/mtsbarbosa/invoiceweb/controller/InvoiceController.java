package net.mtsbarbosa.invoiceweb.controller;

import net.mtsbarbosa.invoiceweb.lang.DuplicatedException;
import net.mtsbarbosa.invoiceweb.service.InvoiceService;
import net.mtsbarbosa.invoiceweb.vo.BaseListResponseVO;
import net.mtsbarbosa.invoiceweb.vo.BaseSingleResponseVO;
import net.mtsbarbosa.invoiceweb.vo.InvoiceOverviewItemVO;
import net.mtsbarbosa.invoiceweb.vo.InvoiceOverviewPostRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/v1")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoices/overview")
    public ResponseEntity<?> getOverviewInvoices() {
        BaseListResponseVO<InvoiceOverviewItemVO> response = new BaseListResponseVO<InvoiceOverviewItemVO>();
        try{
            response.setBody(invoiceService.getOverviewInvoices());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/invoices/overview")
    public ResponseEntity<?> postOverviewInvoice(@Valid @RequestBody InvoiceOverviewPostRequestVO invoiceOverviewPostRequest){
        BaseSingleResponseVO<InvoiceOverviewItemVO> response = new BaseSingleResponseVO<>();
        try{
            response.setBody(invoiceService.insertOverviewInvoice(invoiceOverviewPostRequest));
            return ResponseEntity.ok(response);
        }catch (DuplicatedException e){
            response.setMessages(
                        Stream.of(e.getMessage())
                                .collect(Collectors.toCollection(HashSet::new)));
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
