package net.mtsbarbosa.invoiceweb.repository;

import net.mtsbarbosa.invoiceweb.model.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, String> {
}
