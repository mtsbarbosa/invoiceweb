package net.mtsbarbosa.invoiceweb.repository;

import net.mtsbarbosa.invoiceweb.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    Optional<Item> findFirstByName(String name);
}
