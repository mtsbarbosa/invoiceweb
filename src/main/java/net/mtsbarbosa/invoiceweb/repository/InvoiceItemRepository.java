package net.mtsbarbosa.invoiceweb.repository;

import net.mtsbarbosa.invoiceweb.model.Invoice;
import net.mtsbarbosa.invoiceweb.model.InvoiceItem;
import net.mtsbarbosa.invoiceweb.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvoiceItemRepository extends CrudRepository<InvoiceItem, Long> {
    Optional<InvoiceItem> findByInvoiceAndItem(Invoice invoice, Item item);
}
