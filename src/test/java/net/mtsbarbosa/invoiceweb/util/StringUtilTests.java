package net.mtsbarbosa.invoiceweb.util;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

class StringUtilTests {

    @Test
    void testCapitalizeFirst(){
        //if null return empty string
        Assert.isTrue(StringUtil.capitalizeFirst(null).equals(""), "Should return \"\" for null parameter");

        //if empty return empty also
        Assert.isTrue(StringUtil.capitalizeFirst("").equals(""), "Should return \"\" for null parameter");

        //if single letter, return uppercase
        Assert.isTrue(StringUtil.capitalizeFirst("a").equals("A"), "Should return \"A\" for a as parameter");
        Assert.isTrue(StringUtil.capitalizeFirst("A").equals("A"), "Should return \"A\" for A as parameter");

        //if more than two letters, return only first uppercase
        Assert.isTrue(StringUtil.capitalizeFirst("test").equals("Test"), "Should return \"Test\" for test as parameter");
        Assert.isTrue(StringUtil.capitalizeFirst("tEST").equals("Test"), "Should return \"Test\" for tEST as parameter");
        Assert.isTrue(StringUtil.capitalizeFirst("tEsT").equals("Test"), "Should return \"Test\" for tEsT as parameter");
        Assert.isTrue(StringUtil.capitalizeFirst("TesT").equals("Test"), "Should return \"Test\" for TesT as parameter");
        Assert.isTrue(StringUtil.capitalizeFirst("test SeCond").equals("Test second"), "Should return \"Test second\" for test SeCond as parameter");
    }

}
