package net.mtsbarbosa.invoiceweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.mtsbarbosa.invoiceweb.model.Invoice;
import net.mtsbarbosa.invoiceweb.repository.CustomerRepository;
import net.mtsbarbosa.invoiceweb.repository.InvoiceItemRepository;
import net.mtsbarbosa.invoiceweb.repository.InvoiceRepository;
import net.mtsbarbosa.invoiceweb.repository.ItemRepository;
import net.mtsbarbosa.invoiceweb.service.InvoiceService;
import net.mtsbarbosa.invoiceweb.vo.InvoiceOverviewItemVO;
import net.mtsbarbosa.invoiceweb.vo.InvoiceOverviewPostRequestVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InvoiceControllerTest {
    private static final String I_OVERVIEW_PREFIX = "/api/v1/invoices/overview";

    private final String OLD_CUSTOMER_NAME = "Paul Fischer";
    private final String NEW_CUSTOMER_NAME = "Fischer & Fischer";
    private final String REF = "D123456789";
    private final String DATE = "01.02.2020";
    private final BigDecimal AMOUNT = new BigDecimal("39.92");
    private final String NEW_ITEM_NAME = "P-S.PCODEAIICEBERG-Z";
    private final String OLD_ITEM_NAME = "P-S.RD-Y";
    private final String CURRENCY = "Euro";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InvoiceItemRepository invoiceItemRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private InvoiceOverviewPostRequestVO request = new InvoiceOverviewPostRequestVO(null, DATE, REF, AMOUNT, OLD_ITEM_NAME,1, CURRENCY);

    /*****************************************************************
     _______  _______ _________            _______  _______  _______
    (  ____ \(  ____ \\__   __/       /\  / ___   )(  __   )(  __   )
    | (    \/| (    \/   ) (         / /  \/   )  || (  )  || (  )  |
    | |      | (__       | |        / /       /   )| | /   || | /   |
    | | ____ |  __)      | |       / /      _/   / | (/ /) || (/ /) |
    | | \_  )| (         | |      / /      /   _/  |   / | ||   / | |
    | (___) || (____/\   | |     / /      (   (__/\|  (__) ||  (__) |
    (_______)(_______/   )_(     \/       \_______/(_______)(_______)
     *****************************************************************/
    @Test
    public void performGetInvoiceOverviewTest() throws Exception{
        //should return 6 entries
        this.mvc.perform(get(I_OVERVIEW_PREFIX)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(
                        content()
                        .string(equalTo("{\"messages\":[]," +
                                "\"body\":[" +
                                "{\"customerName\":\"Paul Fischer\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"24.01.2020\"," +
                                    "\"ref\":\"D345556560\"," +
                                    "\"amount\":350.43," +
                                    "\"itemName\":\"TCA-NS\"," +
                                    "\"quantity\":1," +
                                    "\"currency\":\"Euro\"}," +
                                "{\"customerName\":\"Paul Fischer\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"24.01.2020\"," +
                                    "\"ref\":\"D345556560\"," +
                                    "\"amount\":350.43," +
                                    "\"itemName\":\"P-S.PS-Y\"," +
                                    "\"quantity\":6," +
                                    "\"currency\":\"Euro\"}," +
                                "{\"customerName\":\"John McDonald\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"24.01.2020\"," +
                                    "\"ref\":\"D345556564\"," +
                                    "\"amount\":2456.09," +
                                    "\"itemName\":\"P-S.RD-Y\"," +
                                    "\"quantity\":1," +
                                    "\"currency\":\"United States Dollar\"}," +
                                "{\"customerName\":\"Paul Fischer\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"05.02.2020\"," +
                                    "\"ref\":\"D845556567\"," +
                                    "\"amount\":12," +
                                    "\"itemName\":\"TCA-NS\"," +
                                    "\"quantity\":10," +
                                    "\"currency\":\"Euro\"}," +
                                "{\"customerName\":\"Paul Fischer\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"05.02.2020\"," +
                                    "\"ref\":\"D845556567\"," +
                                    "\"amount\":12," +
                                    "\"itemName\":\"P-S.PCODEAIICEBERG-Y\"," +
                                    "\"quantity\":5," +
                                    "\"currency\":\"Euro\"}," +
                                "{\"customerName\":\"Paul Fischer\"," +
                                    "\"transactionType\":\"Invoice\"," +
                                    "\"date\":\"05.02.2020\"," +
                                    "\"ref\":\"D845556567\"," +
                                    "\"amount\":12," +
                                    "\"itemName\":\"C-S.RRR-Y\"," +
                                    "\"quantity\":1," +
                                    "\"currency\":\"Euro\"}]}")));
    }

    /***************************************************************************
     _______  _______  _______ _________               ___    _______     ___
    (  ____ )(  ___  )(  ____ \\__   __/       /\     /   )  (  __   )   /   )
    | (    )|| (   ) || (    \/   ) (         / /    / /) |  | (  )  |  / /) |
    | (____)|| |   | || (_____    | |        / /    / (_) (_ | | /   | / (_) (_
    |  _____)| |   | |(_____  )   | |       / /    (____   _)| (/ /) |(____   _)
    | (      | |   | |      ) |   | |      / /          ) (  |   / | |     ) (
    | )      | (___) |/\____) |   | |     / /           | |  |  (__) |     | |
    |/       (_______)\_______)   )_(     \/            (_)  (_______)     (_)

     *****************************************************************************/
    @Test
    public void performQuantityBadFormatTest() throws Exception {
        //no quantity should return BAD REQUEST
        request.setQuantity(0);
        performBadRequestTest(request);
    }

    @Test
    public void performItemNameBadFormatTest() throws Exception {
        //no item should return BAD REQUEST
        request.setItemName(null);
        performBadRequestTest(request);

        //empty customer should return BAD REQUEST
        request.setItemName("");
        performBadRequestTest(request);
    }

    @Test
    public void performAmountBadFormatTest() throws Exception {
        //no amount should return BAD REQUEST
        request.setAmount(null);
        performBadRequestTest(request);

        //zero amount should return BAD REQUEST
        request.setAmount(BigDecimal.ZERO);
        performBadRequestTest(request);
        request.setAmount(AMOUNT);
    }

    @Test
    public void performRefBadFormatTest() throws Exception {
        //no ref should return BAD REQUEST
        request.setRef(null);
        performBadRequestTest(request);

        //empty ref should return BAD REQUEST
        request.setRef("");
        performBadRequestTest(request);

        //short ref should return BAD REQUEST
        request.setRef("D");
        performBadRequestTest(request);

        //too big ref should return BAD REQUEST
        request.setRef("D1234567890000");
        performBadRequestTest(request);
        request.setRef(REF);
    }

    @Test
    public void performDateBadFormatTest() throws Exception {
        //no date should return BAD REQUEST
        request.setDate(null);
        performBadRequestTest(request);

        //bad format date should return BAD REQUEST
        request.setDate("03/2020");
        performBadRequestTest(request);
        request.setDate("2020/01");
        performBadRequestTest(request);
        request.setDate("2020");
        performBadRequestTest(request);
        request.setDate("1/1/2020");
        performBadRequestTest(request);
        request.setDate("2020/01/04");
        performBadRequestTest(request);

        //empty date should return BAD REQUEST
        request.setDate("");
        performBadRequestTest(request);
        request.setDate(DATE);
    }

    @Test
    public void performCustomerBadFormatTest() throws Exception {
        //no customer should return BAD REQUEST
        request.setCustomerName(null);
        performBadRequestTest(request);

        //empty customer should return BAD REQUEST
        request.setCustomerName("");
        performBadRequestTest(request);
        request.setCustomerName(OLD_CUSTOMER_NAME);
    }

    @Test
    public void performDuplicatedBadFormatTest() throws Exception {
        request.setCustomerName(OLD_CUSTOMER_NAME);
        request.setItemName("TCA-NS");
        request.setRef("D345556560");
        performBadRequestTest(request);

        request.setCustomerName(OLD_CUSTOMER_NAME);
        request.setItemName(OLD_ITEM_NAME);
        request.setRef(REF);
    }

    private void performBadRequestTest(InvoiceOverviewPostRequestVO request) throws Exception {
        this.mvc.perform(post(I_OVERVIEW_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**************************************************************************
     _______  _______  _______ _________            _______  _______  _______
    (  ____ )(  ___  )(  ____ \\__   __/       /\  / ___   )(  __   )(  __   )
    | (    )|| (   ) || (    \/   ) (         / /  \/   )  || (  )  || (  )  |
    | (____)|| |   | || (_____    | |        / /       /   )| | /   || | /   |
    |  _____)| |   | |(_____  )   | |       / /      _/   / | (/ /) || (/ /) |
    | (      | |   | |      ) |   | |      / /      /   _/  |   / | ||   / | |
    | )      | (___) |/\____) |   | |     / /      (   (__/\|  (__) ||  (__) |
    |/       (_______)\_______)   )_(     \/       \_______/(_______)(_______)
     *************************************************************************/
    @Test
    public void performPostSuccessOldDataTest() throws Exception{
        //should successfully insert invoice data, but customer and item because they're exiting
        request.setCustomerName(OLD_CUSTOMER_NAME);
        request.setItemName(OLD_ITEM_NAME);

        final Long oldCustomerId = customerRepository.findFirstByName(OLD_CUSTOMER_NAME).get().getId();
        final Long oldItemId = itemRepository.findFirstByName(OLD_ITEM_NAME).get().getId();

        this.mvc.perform(post(I_OVERVIEW_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(status().isOk());

        List<InvoiceOverviewItemVO> invoiceOverviewItems = invoiceService.getOverviewInvoices();
        Assert.isTrue(invoiceOverviewItems.size() == 7, "Extra invoice overview was not being output or was not added");
        Assert.isTrue(invoiceOverviewItems
                            .stream()
                            .anyMatch(item -> {
                                return item.getCustomerName().equals(OLD_CUSTOMER_NAME)
                                        && item.getItemName().equals(OLD_ITEM_NAME)
                                        && item.getRef().equals(REF);
                            }), "Extra invoice overview was not being output or was not added");

        //customer id should be the same and no error should be thrown
        Assert.isTrue(customerRepository.findFirstByName(OLD_CUSTOMER_NAME).get().getId() == oldCustomerId,
                        "old customer id is not matching after post");

        //item id should be the same and no error should be thrown
        Assert.isTrue(itemRepository.findFirstByName(OLD_ITEM_NAME).get().getId() == oldItemId,
                        "old item id is not matching after post");

        //Invoice should be inserted since its a new REF
        Optional<Invoice> invoice = invoiceRepository.findById(REF);
        Assert.isTrue(invoice.isPresent(), "Invoice was not inserted");

        invoiceService.deleteCascadeInvoice(invoice.get());
    }

    @Test
    public void performPostSuccessNewDataTest() throws Exception{
        //should successfully insert invoice data, but customer and item because they're exiting
        request.setCustomerName(NEW_CUSTOMER_NAME);
        request.setItemName(NEW_ITEM_NAME);

        this.mvc.perform(post(I_OVERVIEW_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(status().isOk());

        List<InvoiceOverviewItemVO> invoiceOverviewItems = invoiceService.getOverviewInvoices();
        Assert.isTrue(invoiceOverviewItems.size() == 7, "Extra invoice overview was not being output or was not added");
        Assert.isTrue(invoiceOverviewItems
                .stream()
                .anyMatch(item -> {
                    return item.getCustomerName().equals(NEW_CUSTOMER_NAME)
                            && item.getItemName().equals(NEW_ITEM_NAME)
                            && item.getRef().equals(REF);
                }), "Extra invoice overview was not being output or was not added");

        //new customer must have been added
        Assert.isTrue(customerRepository.findFirstByName(NEW_CUSTOMER_NAME).isPresent(),"new customer was not inserted");

        //item id should be the same and no error should be thrown
        Assert.isTrue(itemRepository.findFirstByName(NEW_ITEM_NAME).isPresent(),"new item was not inserted");

        //Invoice should be inserted since its a new REF
        Optional<Invoice> invoice = invoiceRepository.findById(REF);
        Assert.isTrue(invoice.isPresent(), "Invoice was not inserted");

        invoiceService.deleteCascadeInvoice(invoice.get());
    }
}
