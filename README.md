# Invoice Web
Technologies involved:
*  Server:
    * Java + SpringBoot + H2 Database
*  Client
    * React / Typescript / Jest / Axios / React-Router-Dom / Material-UI 

## 1. Tests
### a. Server
* mvn test
### b. Client
* cd client && npm test

## 2. Building
### a. Server
If you prefer not to build, use jar under /bin, otherwise:

#### I. Pre-requisites:
* Java >= 8
* Maven

#### II. Executing build
* ./mvnw package

## 3. Running
### a. Server
#### I. Pre-requisites:
* Java >= 8
#### II. Running from build
* java -jar target/invoiceweb-0.0.1-SNAPSHOT.jar
#### II. Running from bin
* java -jar bin/invoiceweb-0.0.1-SNAPSHOT.jar

### b. Client
#### I. Pre-requisites:
* Node >= 12
#### II. Running
* cd client
* npm install
* npm start

## 4. Quick Links:
### DEV:
* Server: http://localhost:8080/api/v1/invoices/overview
* Client: http://localhost:3000

## 5. Troubleshooting
* a. H2 Database in server side uses this path for storing files: ${HOME}/data/invoicewebdev <br/> make sure java has admin permissions or access to that folder. <br/> If necessary, create that path previously and give permissions
* b. It was tested in Linux and MAC only, be aware that there is no official support for Windows or any other OS


## 6. Documentation
* The api is documented under src/main/resources/static/v1/api.yaml
* No authentication is required to access the api or the client


## 7. Contact
* For issues, questions, suggestions: <Mateus Carvalho> mateusbcarvalho@outlook.com


