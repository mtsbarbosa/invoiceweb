import {convertToInternationalDateString} from "../../../src/utils/Date";


test("date@convertToInternationalDateString should return right formatted date", () => {
    let date: Date;

    date = new Date("05/01/2020");
    expect(convertToInternationalDateString(date, ".")).toBe("01.05.2020");

    date = new Date("01/01/2020");
    expect(convertToInternationalDateString(date, ".")).toBe("01.01.2020");

    date = new Date("01/31/2000");
    expect(convertToInternationalDateString(date, ".")).toBe("31.01.2000");
});