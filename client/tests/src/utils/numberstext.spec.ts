import {allowMoney, allowPositivesNonZero, DEFAULT_MONEY_VALUE} from "../../../src/utils/NumbersText";

let valueToTest = 0;
const posFunction: Function = (value: any) => {
    valueToTest = value;
};

beforeEach(() => {
    valueToTest = 0;
});

test('numberstext@allowPositivesNonZero should not allow negatives', () => {
    allowPositivesNonZero("-1",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("-10",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("-01",posFunction);
    expect(valueToTest).toBe(1);
});

test('numberstext@allowPositivesNonZero should not allow non digits', () => {
    allowPositivesNonZero("a",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("#",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("%",posFunction);
    expect(valueToTest).toBe(1);
});

test('numberstext@allowPositivesNonZero should not allow zero-starting numbers', () => {
    allowPositivesNonZero("01",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("002",posFunction);
    expect(valueToTest).toBe(1);
});

test('numberstext@allowPositivesNonZero should not allow float numbers', () => {
    allowPositivesNonZero("1.1",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("56.01",posFunction);
    expect(valueToTest).toBe(1);

    allowPositivesNonZero("1,1",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("89,10",posFunction);
    expect(valueToTest).toBe(1);
});

test('numberstext@allowPositivesNonZero should not allow zero', () => {
    allowPositivesNonZero("0",posFunction);
    expect(valueToTest).toBe(1);
});

test('numberstext@allowPositivesNonZero should allow positive numbers', () => {
    allowPositivesNonZero("1",posFunction);
    expect(valueToTest).toBe(1);
    valueToTest = 0;

    allowPositivesNonZero("23",posFunction);
    expect(valueToTest).toBe(23);
    valueToTest = 0;

    allowPositivesNonZero("100",posFunction);
    expect(valueToTest).toBe(100);
});

test('numberstext@allowMoney should not allow non digits', () => {
    allowMoney(".",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney("+12",posFunction);
    expect(valueToTest).toBe("0.12");
    valueToTest = 0;

    allowMoney("+1+2",posFunction);
    expect(valueToTest).toBe("0.12");
    valueToTest = 0;

    allowMoney("-1#2",posFunction);
    expect(valueToTest).toBe("0.12");
    valueToTest = 0;

    allowMoney("aaa",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney(".,",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);

    allowMoney("",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
});

test('numberstext@allowMoney should add digits for single digit', () => {
    allowMoney("1",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney("-1",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney(".1",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney(",1",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);

    allowMoney("4",posFunction);
    expect(valueToTest).toBe("0.04");
    valueToTest = 0;

    allowMoney("0",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
});

test('numberstext@allowMoney should add digits for two digits input', () => {
    allowMoney("10",posFunction);
    expect(valueToTest).toBe("0.10");
    valueToTest = 0;

    allowMoney("-10",posFunction);
    expect(valueToTest).toBe("0.10");
    valueToTest = 0;

    allowMoney(".10",posFunction);
    expect(valueToTest).toBe("0.10");
    valueToTest = 0;

    allowMoney(",10",posFunction);
    expect(valueToTest).toBe("0.10");

    allowMoney("40",posFunction);
    expect(valueToTest).toBe("0.40");
    valueToTest = 0;

    allowMoney("0023",posFunction);
    expect(valueToTest).toBe("0.23");
    valueToTest = 0;

    allowMoney("00",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
});

test('numberstext@allowMoney should keep values for right format', () => {
    allowMoney("10.00",posFunction);
    expect(valueToTest).toBe("10.00");
    valueToTest = 0;

    allowMoney("2.00",posFunction);
    expect(valueToTest).toBe("2.00");
    valueToTest = 0;

    allowMoney("3.01",posFunction);
    expect(valueToTest).toBe("3.01");
    valueToTest = 0;

    allowMoney("100.56",posFunction);
    expect(valueToTest).toBe("100.56");
});

test('numberstext@allowMoney should add point to >= 3 digits input', () => {
    allowMoney("100",posFunction);
    expect(valueToTest).toBe("1.00");
    valueToTest = 0;

    allowMoney("2000",posFunction);
    expect(valueToTest).toBe("20.00");
    valueToTest = 0;

    allowMoney("56700",posFunction);
    expect(valueToTest).toBe("567.00");
    valueToTest = 0;

    allowMoney("000",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
    valueToTest = 0;

    allowMoney("0000",posFunction);
    expect(valueToTest).toBe(DEFAULT_MONEY_VALUE);
});