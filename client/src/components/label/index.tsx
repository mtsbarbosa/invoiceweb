import React from "react";

export interface LabelParamProps {
    text: string
}

type Props = LabelParamProps;

export default function Label(props: Props) {
    return (
        <div className="MuiFormControl-root MuiTextField-root">
            <label
                className="MuiFormLabel-root MuiInputLabel-root MuiInputLabel-formControl MuiInputLabel-animated MuiInputLabel-shrink Mui-focused Mui-focused"
                data-shrink="true" htmlFor="standard-basic" id="standard-basic-label" style={{ color: "rgba(0, 0, 0, 0.54)", whiteSpace: "nowrap" }}>
                {props.text}
            </label>
        </div>
    );
}