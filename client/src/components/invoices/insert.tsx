import React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider,} from '@material-ui/pickers';
import InvoicesService, {CurrencyTypes, InvoiceOverview} from "../../services/invoices";
import Label from "../label";
import NativeSelect from '@material-ui/core/NativeSelect';
import {allowMoney, allowPositivesNonZero, DEFAULT_MONEY_VALUE} from "../../utils/NumbersText";
import FormControl from '@material-ui/core/FormControl';
import {convertToInternationalDateString} from "../../utils/Date";
import {getMapValidationErrors, PostResponse} from "../../services/BaseService";
import ValidationError from "../validation_error";

interface EventProps {
    onSaveInvoiceOverviewItem: Function,
}

type Props = EventProps;

export default function InvoicesInsert(props: Props) {
    const initialErrors: Map<string, boolean> = new Map<string, boolean>([
        ["customerName", false],
        ["date", false],
        ["ref", false],
        ["amount", false],
        ["itemName", false],
        ["quantity", false],
        ["currency", false],
    ]);
    const initialErrorMessages: Map<string, string> = new Map<string, string>();
    const [open, setOpen] = React.useState(false);
    const [errors, setErrors] = React.useState(initialErrors);
    const [errorMessages, setErrorMessages] = React.useState(initialErrorMessages);

    //field states
    const [customerName, setCustomerName] = React.useState("");
    const [date, setDate] = React.useState(new Date());
    const [ref, setRef] = React.useState("");
    const [amount, setAmount] = React.useState(DEFAULT_MONEY_VALUE);
    const [itemName, setItemName] = React.useState("");
    const [quantity, setQuantity] = React.useState(1);
    const [currency, setCurrency] = React.useState(CurrencyTypes.EUR.toString());

    const getModel = () : InvoiceOverview => {
        return {
            amount: amount,
            currency: currency.toString(),
            customerName: customerName,
            date: convertToInternationalDateString(date, "."),
            itemName: itemName,
            quantity: quantity,
            ref: ref,
            transactionType: "Invoice",
        };
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const resetErrors = () => {
        setErrorMessages(new Map<string, string>());
        for(let key of Array.from( errors.keys()) ) {
            errors.set(key, false);
        }
    };

    const resetModel = () => {
        setCustomerName("");
        setDate(new Date());
        setRef("");
        setAmount(DEFAULT_MONEY_VALUE);
        setItemName("");
        setQuantity(1);
        setCurrency(CurrencyTypes.EUR.toString());
    }

    const highLightErrors = ( validationErrors: Map<string, string> ) => {
        let displayedErrors: Map<string, boolean> = new Map<string, boolean>();
        Array.from(validationErrors.keys()).map( (key: string) => displayedErrors.set(key, true));
        setErrors(displayedErrors);
    };

    const handleSave = () => {
        (async function postInvoiceOverviewItem() {
            const model: InvoiceOverview = getModel();
            const resp: PostResponse = (await InvoicesService.postInvoiceOverview(model));
            resetErrors();
            if(!resp.error){
                props.onSaveInvoiceOverviewItem(model);
                setOpen(false);
                resetModel();
            }else{
                const validationErrors: Map<string, string> = getMapValidationErrors(resp);
                setErrorMessages(validationErrors);
                highLightErrors(validationErrors);
            }
        })();
    };

    const handleCustomerNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const customerName: string = event.target.value;
        setCustomerName(customerName);
    };

    const handleDateChange = (date: Date | null) => {
        setDate(date? date : new Date());
    };

    const handleRefChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const ref: string = event.target.value;
        setRef(ref);
    };

    const handleAmountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const text: string = event.target.value;
        allowMoney(text, setAmount);
    };

    const handleItemNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const itemName: string = event.target.value;
        setItemName(itemName);
    };

    const handleQuantityChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const text: string = event.target.value;
        allowPositivesNonZero(text, setQuantity);
    };

    const handleCurrencyChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
        const currency: string = event.target.value as string;
        setCurrency(currency);
    };

    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                New Invoice Item
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">New Invoice Item</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item md={12}>
                            <ValidationError messages={errorMessages}/>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextField
                                id="customerName"
                                label="Customer Name"
                                error={errors.get("customerName")}
                                value={customerName}
                                onChange={handleCustomerNameChange}
                                inputProps={{
                                    maxLength: 150,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Label text="Transaction Type" />
                            <div
                                style={{ marginTop: "4px" }}>
                                Invoice
                            </div>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    id="date"
                                    label="Date"
                                    format="dd.MM.yyyy"
                                    error={errors.get("date")}
                                    value={date}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextField
                                id="ref"
                                label="Ref#"
                                error={errors.get("ref")}
                                value={ref}
                                onChange={handleRefChange}
                                inputProps={{
                                    maxLength: 10,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextField
                                id="amount"
                                label="Amount"
                                error={errors.get("amount")}
                                value={amount}
                                onChange={handleAmountChange}
                                inputProps={{
                                    maxLength: 150,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextField
                                id="itemName"
                                label="Item Name"
                                error={errors.get("itemName")}
                                value={itemName}
                                onChange={handleItemNameChange}
                                inputProps={{
                                    maxLength: 150,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextField
                                id="quantity"
                                type="number"
                                label="Quantity"
                                error={errors.get("quantity")}
                                value={quantity}
                                onChange={handleQuantityChange}
                                inputProps={{
                                    maxLength: 150,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <FormControl error={errors.get("currency")}>
                                <Label text="Currency"/>
                                <NativeSelect
                                    value={currency.toString()}
                                    onChange={handleCurrencyChange}
                                    name="currency"
                                    inputProps={{
                                        id: 'currency',
                                    }}
                                    style={{
                                        marginTop: "15px",
                                    }}
                                >
                                    {Object.entries(CurrencyTypes).map((entry: string[]) =>
                                        <option key={entry[0]} value={entry[1]}>{entry[1]}</option>
                                    )}
                                </NativeSelect>
                            </FormControl>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}