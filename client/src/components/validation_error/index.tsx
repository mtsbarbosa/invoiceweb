import React from "react";
import useStyles from "../../theme/theme";

interface AttributeProps{
    messages: Map<string, string>,
}

type Props = AttributeProps;

export default function ValidationError(props: Props) {
    const classes = useStyles();

    return (
        <div>
            {props.messages.size > 0 &&
                <div className={classes.message_container}>
                    {Array.from( props.messages.keys()).map( (key: string) =>
                        <div key={key}><span>{key}:</span>&nbsp;<span>{props.messages.get(key)}</span></div>
                    )}
                </div>
            }
        </div>
    );
}