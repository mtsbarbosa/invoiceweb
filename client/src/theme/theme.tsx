import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            maxWidth: 752,
        },
        wrapper: {
        },
        title: {
            margin: theme.spacing(4, 0, 2),
            textAlign: "center",
            color: "#e0e0e0",
        },
        listItem: {
            border: 0,
            borderRadius: 3,
            backgroundColor: "#e0e0e0",
        },
        listItemText: {
            textAlign: "center",
        },
        clickableItem:{
            '&:hover': {
                transform: "scale(1.02)",
                opacity: .9,
                transition: "0.2s",
                cursor: "pointer",
            },
        },
        link:{
            color: "#424242",
            textDecoration: "none",
        },
        message_container:{
            borderRadius: "4px 4px 4px 4px",
            backgroundColor: "#f98f8f",
            padding: "10px",
            fontSize: "10px",
            color: "#000000"
        }
    }),
);

export default useStyles;