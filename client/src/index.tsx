import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Invoices from "./pages/Invoices";

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route path="/invoices" exact={true} component={Invoices} />
        </Switch>
    </ BrowserRouter>, document.getElementById('root'));
