export interface PostResponse {
    error: boolean,
    response: any,
}

export function getMapValidationErrors(postResponse: PostResponse): Map<string, string> {
    let validationErrors: Map<string, string> = new Map<string, string>();
    try {
        postResponse.response.data.errors.forEach((error: any) => {
            validationErrors.set(error.field, error.defaultMessage);
        });
    }catch(error){
        console.log("ERROR","error trying to retrieve validation errors => "+ error);
    }
    return validationErrors;
}