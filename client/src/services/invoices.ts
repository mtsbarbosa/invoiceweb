import api from "./api";
import {PostResponse} from "./BaseService";

export interface InvoiceOverview{
    customerName: string,
    transactionType: string,
    date: string,
    ref: string,
    amount: string,
    itemName: string,
    quantity: number,
    currency: string,
}

export enum CurrencyTypes {
    EUR = "Euro",
    USD = "United States Dollar",
}

export default class InvoicesService {
    private static readonly INVOICE_OVERVIEW_PATH: string = "/invoices/overview";

    public static async loadInvoiceOverviews(): Promise<InvoiceOverview[]>{
        try{
            const { data } = await api.get(this.INVOICE_OVERVIEW_PATH);
            return data.body;
        }catch (error) {
            console.log(error);
        }
        return [];
    }

    public static async postInvoiceOverview(invoiceOverview : InvoiceOverview): Promise<PostResponse>{
        try{
            await api.post(this.INVOICE_OVERVIEW_PATH, invoiceOverview);
            return { error: false, response: "" };
        }catch (error) {
            return { error: true, response: error.response };
        }
    }
}