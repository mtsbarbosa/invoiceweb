import React, {useEffect, useState} from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Box from '@material-ui/core/Box';
import useStyles from "../theme/theme";
import InvoicesService, {InvoiceOverview} from "../services/invoices";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import InvoicesInsert from "../components/invoices/insert";

export default function Invoices() {
    const [invoiceOverviews, setInvoiceOverview] = useState<InvoiceOverview[]>([]);
    const classes = useStyles();

    useEffect(() => {
        function handleInvoiceOverviewChange(invoiceOverviews: InvoiceOverview[]) {
            setInvoiceOverview(invoiceOverviews);
        }
        (async function loadInvoiceOverviews() {
            handleInvoiceOverviewChange(await InvoicesService.loadInvoiceOverviews());
        })();
    }, []);

    const onReceiveNewInvoiceOverviewItem = (invoiceOverview: InvoiceOverview) => {
        setInvoiceOverview([...invoiceOverviews, invoiceOverview]);
    };

    return(
        <Container>
            <Box display="flex" p={1} justifyContent="center">
                <Grid container spacing={2}>
                    <Grid item xs={12} md={12}>
                        <Typography variant="h3" className={classes.title}>
                            Invoices
                        </Typography>
                        <div className={classes.wrapper}>
                            <InvoicesInsert
                                onSaveInvoiceOverviewItem={onReceiveNewInvoiceOverviewItem}
                            /><br />
                            <TableContainer component={Paper}>
                                <Table aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="left">Customer Name</TableCell>
                                            <TableCell align="left">Transaction Type</TableCell>
                                            <TableCell align="left">Date</TableCell>
                                            <TableCell align="right">Ref#</TableCell>
                                            <TableCell align="right">Amount</TableCell>
                                            <TableCell align="left">Item Name</TableCell>
                                            <TableCell align="right">Quantity</TableCell>
                                            <TableCell align="left">Currency</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        { invoiceOverviews.map(iOverview => (
                                            <TableRow key={`${iOverview.ref}_${iOverview.itemName}`}>
                                                <TableCell component="th" align="left" scope="row">
                                                    {iOverview.customerName}
                                                </TableCell>
                                                <TableCell component="th" align="left" scope="row">
                                                    {iOverview.transactionType}
                                                </TableCell>
                                                <TableCell component="th" align="left" scope="row">
                                                    {iOverview.date}
                                                </TableCell>
                                                <TableCell component="th" align="right" scope="row">
                                                    {iOverview.ref}
                                                </TableCell>
                                                <TableCell component="th" align="right" scope="row">
                                                    {iOverview.amount}
                                                </TableCell>
                                                <TableCell component="th" align="left" scope="row">
                                                    {iOverview.itemName}
                                                </TableCell>
                                                <TableCell component="th" align="right" scope="row">
                                                    {iOverview.quantity}
                                                </TableCell>
                                                <TableCell component="th" align="left" scope="row">
                                                    {iOverview.currency}
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
}
