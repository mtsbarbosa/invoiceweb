import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Container from "@material-ui/core/Container";
import ListItemText from "@material-ui/core/ListItemText";
import Box from '@material-ui/core/Box';
import useStyles from "../theme/theme";
import { Link } from "react-router-dom";

export default function Home(){
    const classes = useStyles();
    const [dense, setDense] = React.useState(false);

    return(
        <Container>
            <Box display="flex" p={1} justifyContent="center">
                <Grid container spacing={2}>
                    <Grid item xs={12} md={12}>
                        <Typography variant="h3" className={classes.title}>
                            Web Invoice
                        </Typography>
                        <div className={classes.wrapper}>
                            <List dense={dense}>
                                <Link to="/invoices" className={classes.link}>
                                    <ListItem
                                        className={`${classes.listItem} ${classes.clickableItem}`}
                                    >
                                        <ListItemText
                                            primary="Invoices"
                                            className={classes.listItemText}
                                        />
                                    </ListItem>
                                </Link>
                            </List>
                        </div>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    );
}