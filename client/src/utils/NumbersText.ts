import {MONEY, NON_DIGIT, NUMBER_ONLY, ZERO} from "./Regex";

export const DEFAULT_MONEY_VALUE: string = "0.01";

export function allowPositivesNonZero(text: string, posFunction: Function) {
    if(!text.match(NUMBER_ONLY)){
        posFunction(1);
    }else if(Number(text) < 1){
        posFunction(1);
    }else{
        posFunction(Number(text));
    }
}

export function allowMoney(text: string, posFunction: Function) {
    if(!text.match(MONEY)){
        if (text.length <= 0) {
            posFunction(DEFAULT_MONEY_VALUE);
        }else{
            //remove non digits
            text = text.split(NON_DIGIT).join("");
            if(text.match(ZERO)){
                posFunction(DEFAULT_MONEY_VALUE);
            }else if(text.length <= 0) {
                posFunction(DEFAULT_MONEY_VALUE);
            } else if (text.length === 1) {
                posFunction(`0.0${text}`);
            } else if (text.length === 2) {
                posFunction(`0.${text}`);
            } else {
                text = text.startsWith("0")? text.substring(1, text.length) : text;
                posFunction(text.substring(0, text.length - 2) + "." + text.substring(text.length - 2, text.length));
            }
        }
    }else{
        //if patter matches money pattern, value will be set directly
        posFunction(text);
    }
}