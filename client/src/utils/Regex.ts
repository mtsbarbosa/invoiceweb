export const NUMBER_ONLY: RegExp = /^([1-9]|[1-9]\d*)$/;
export const MONEY: RegExp = /^(\d+[.][0-9]{2})$/;
export const ZERO: RegExp = /^([0]*)$/;
export const NON_DIGIT: RegExp = /[^0-9]/;