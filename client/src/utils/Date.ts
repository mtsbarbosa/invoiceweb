export function convertToInternationalDateString(date: Date, separator: string): string {
    let day: number = date.getDate();
    let month: number = date.getMonth() + 1;
    return `${day < 10? "0" + day : day.toString()}${separator}${month < 10? "0" + month : month.toString()}${separator}${date.getFullYear()}`;
}